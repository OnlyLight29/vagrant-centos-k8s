#!/bin/bash

# Add host
echo "100.0.0.1 master.jhooq.com master" >> /etc/hosts
echo "100.0.0.2 node01.jhooq.com node01" >> /etc/hosts
echo "100.0.0.3 node02.jhooq.com node02" >> /etc/hosts

# update yum repos
sudo yum install -y yum-utils

# install docker
sudo yum-config-manager \
  --add-repo \
  https://download.docker.com/linux/centos/docker-ce.repo

sudo yum update -y

sudo yum install docker-ce docker-ce-cli containerd.io -y

sudo rm /etc/containerd/config.toml

sudo usermod -aG docker $(whoami)

sudo systemctl restart containerd
sudo systemctl enable docker
sudo systemctl restart docker

# Disable SELinux
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

# Disable CentOS firewall
sudo systemctl disable firewalld
sudo systemctl stop firewalld

# Disable swapping
sudo swapoff -a
sudo bash -c 'echo "net.bridge.bridge-nf-call-ip6tables = 1" > /etc/sysctl.d/k8s.conf'
sudo bash -c 'echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.d/k8s.conf'
sudo sysctl --system

# Add the Kubernetes repo
sudo cat >>/etc/yum.repos.d/kubernetes.repo<<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

sudo yum install -y kubeadm kubelet kubectl

sudo systemctl enable kubelet
sudo systemctl start kubelet
