# Vagrant centos7 Kubernestes practce

Reference: https://jhooq.com/15-steps-to-install-kubernetes-on-bento-centos7/

## Install cluster

Install cluster with shell script settings with 1 node **master** and 1 node **worker**
```
vagrant up
```
## Initialize Control Plane (Only master node)

### Initialize Kubernetes cluster

```
vagrant ssh master
```

### Move kube config file
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

### Install network
```
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.1/manifests/calico.yaml
```

## If have issue
Reload master node
```
vagrant reload master
```

Disable swapping
```
sudo swapoff -a
sudo systemctl enable kubelet
sudo systemctl start kubelet
```

Wait a minute and try `telnet 100.0.0.1 6433` to check connection before join cluster
